const inputSet = document.querySelector(".inputSet")
const inputSubmit = document.querySelector(".inputSubmit")
document.querySelector("form").addEventListener("click", event => {
    showPassword("eye-set-js", "closed-eye-set-js", inputSet)
    showPassword("eye-submit-js", "closed-eye-submit-js", inputSubmit)
    submitPassword(document.querySelector(".result"))
})
function showPassword(openedEyeClassName, closedEyeClassName, input) {
    if (event.target.classList.contains(openedEyeClassName) || event.target.classList.contains(closedEyeClassName)) {
        document.querySelector(`.${openedEyeClassName}`).classList.toggle("active")
        document.querySelector(`.${closedEyeClassName}`).classList.toggle("active")
        if (input.type === "password") {
            input.type = "text";
        } else {
            input.type = "password";
        }
    }
}
function submitPassword(result) {
    if (event.target.classList.contains("btn")) {
        if(inputSet.value === inputSubmit.value && !/\s/.test(inputSet.value) && inputSet.value != ""){
            result.style.color = "green"
            result.innerHTML = `your password is: ${inputSet.value}`
        } else { 
            event.preventDefault() // отменяет отправку данных на сервер и не создаёт объект события event (дефолтных поведений сабмита)
            result.innerHTML = "Incorrect password"
            result.style.color = "red"
        }
    }
} 